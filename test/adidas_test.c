#include "unity.h"
#include "libadidas.h"
#include "utils.h"
#include "lightning.h"

#define NRAND 20

int answers[] = {
        0b0000000,
        0b1010001,
        0b0110010,
        0b1100011,
        0b1100100,
        0b0110101,
        0b1010110,
        0b0000111,
        0b1111000,
        0b0101001,
        0b1001010,
        0b0011011,
        0b0011100,
        0b1001101,
        0b0101110,
        0b1111111
    };

void setUp(void) { ; }

void tearDown(void) { ; }

void test_encode() {

    for (int i = 0; i < 16; i++) {
        TEST_ASSERT_EQUAL(answers[i], adidas_encode(i));
    }

}

void test_correct_decode() {

    for (int i = 0; i < 16; i++) {
        TEST_ASSERT_EQUAL(i, adidas_decode(answers[i]));
    }

}

void test_one_flipped() {

    for (int nbit = 0; nbit < 4; nbit++) {
        for (int i = 0; i < 16; i++) {
            TEST_ASSERT_EQUAL(i, adidas_decode(answers[i] ^ (1 << nbit)));
        }
    }

}

void test_encode_char() {
    uint8_t c = 'a';

    uint16_t res = adidas_encode_char(c);

    TEST_ASSERT_EQUAL(answers[c & 0xF], res & 0xFF);
    TEST_ASSERT_EQUAL(answers[(c >> 4) & 0xF], (res >> 8) & 0xFF);
}

void test_decode_char() {

    uint16_t enc = 0b101011001010001;

    uint8_t res = adidas_decode_char(enc);

    TEST_ASSERT_EQUAL(res, 'a');

}

void test_alphabet_full() {

    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/!?";

    for (char *c = alphabet; *c != '\0'; c++) {
        uint16_t encoded = adidas_encode_char(*c);

        TEST_ASSERT_EQUAL(answers[*c & 0xF], encoded & 0xFF);
        TEST_ASSERT_EQUAL(answers[(*c >> 4) & 0xF], (encoded >> 8) & 0xFF);

        uint8_t decoded = adidas_decode_char(encoded);

        TEST_ASSERT_EQUAL(*c, decoded);
    }

}

void test_file() {

    char *inp_file = "test/test.txt";
    char *enc_file = "test/test.bin";
    char *fin_file = "test/out.txt";

    FILE *inp = fopen(inp_file, "r");
    TEST_ASSERT_NOT_EQUAL(NULL, inp);

    FILE *out = fopen(enc_file, "w+");
    TEST_ASSERT_NOT_EQUAL(NULL, out);

    FILE *fin = fopen(fin_file, "w+");
    TEST_ASSERT_NOT_EQUAL(NULL, fin);

    encodeFile(inp, out);
    fflush(out);
    fseek(out, 0, SEEK_SET);

    decodeFile(out, fin);
    fflush(fin);

    int res = 0;

    fseek(inp, 0, SEEK_SET);
    fseek(fin, 0, SEEK_SET);

    uint8_t c1 = getc(inp);
    uint8_t c2 = getc(fin);

    while (!feof(inp) && !feof(fin)) {
        res |= c1 != c2;
        if (res) break;
        c1 = getc(inp);
        c2 = getc(fin);
    }

    res |= feof(inp) ^ feof(fin);

    remove(enc_file);
    remove(fin_file);

    fclose(inp);
    fclose(out);
    fclose(fin);

    TEST_ASSERT_FALSE(res);
    
}

void test_file_corrupt() {

    char *inp_file = "test/test.txt";
    char *enc_file = "test/test.bin";
    char *fin_file = "test/out.txt";

    FILE *inp = fopen(inp_file, "r");
    TEST_ASSERT_NOT_EQUAL(NULL, inp);

    FILE *out = fopen(enc_file, "w+");
    TEST_ASSERT_NOT_EQUAL(NULL, out);

    FILE *fin = fopen(fin_file, "w+");
    TEST_ASSERT_NOT_EQUAL(NULL, fin);

    encodeFile(inp, out);
    fflush(out);
    fseek(out, 0, SEEK_SET);

    // Corrupt every byte of the file

    lightning_strike(out);

    decodeFile(out, fin);
    fflush(fin);

    int res = 0;

    fseek(inp, 0, SEEK_SET);
    fseek(fin, 0, SEEK_SET);

    uint8_t c1 = getc(inp);
    uint8_t c2 = getc(fin);

    while (!feof(inp) && !feof(fin)) {
        res |= c1 != c2;
        if (res) break;
        c1 = getc(inp);
        c2 = getc(fin);
    }

    res |= feof(inp) ^ feof(fin);

    remove(enc_file);
    remove(fin_file);

    fclose(inp);
    fclose(out);
    fclose(fin);

    TEST_ASSERT_FALSE(res);
    
}

int main() {

    // Program was built with the older version of Unity,
    // This has to be fixed in the newer one.
    // Structure is different.

    // UnityBegin();

    int c = 0;

    RUN_TEST(test_encode, ++c);
    RUN_TEST(test_correct_decode, ++c);
    RUN_TEST(test_one_flipped, ++c);
    RUN_TEST(test_encode_char, ++c);
    RUN_TEST(test_decode_char, ++c);
    RUN_TEST(test_alphabet_full, ++c);
    RUN_TEST(test_file, ++c);
    RUN_TEST(test_file_corrupt, ++c);

    return UnityEnd();
}