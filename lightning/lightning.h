#ifndef LIGHTNING_H
#define LIGHTNING_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

size_t lightning_strike(FILE *f);
#endif