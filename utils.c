#include "utils.h"
// Some useful functions
// For tests and main program

// Prints help info to stdout
void printHelp() {

    fprintf(stdout, "\nUsage:\n./adidas -d|-e [-f FILE]\n\nOptions:\n");
    fprintf(stdout, "-d - decode mode\n");
    fprintf(stdout, "-e - encode mode\n");
    fprintf(stdout, "-i - input file, defaults to stdin\n");
    fprintf(stdout, "-o - output file, defaults to stdout\n");
    fprintf(stdout, "-h - display this message and exit\n");
}

// Encode specified file
// To specified output
void encodeFile(FILE *in_f, FILE *out_f) {

    uint8_t c = fgetc(in_f);

    // While hile has not ended
    // Using feof, as program first reads,
    // Then checks for EOF
    while (!feof(in_f)) {
        uint16_t encoded = adidas_encode_char(c);
        fwrite(&encoded, 2, 1, out_f);
        c = fgetc(in_f);
    }

}

// Decode specified file
// To specified output
void decodeFile(FILE *inp_f, FILE *out_f) {
    uint16_t temp = 0;
        
    // While file has not ended
    while (fread(&temp, 2, 1, inp_f) != 0) {
        // File is garanteed to have even numbery of bytes
        uint8_t decoded = adidas_decode_char(temp);
        fputc(decoded, out_f);

    }
}