UNITY_HOME=./Unity/src/

SHARED=-I libadidas libadidas/adidas.c
TEST=-I $(UNITY_HOME) -I test/ $(UNITY_HOME)/unity.c -I . utils.c -I lightning lightning/lightning.c

PN=adidas
TESTPN=unittest
LPN=strike

CC=gcc

adidas:
	$(CC) -o $(PN) $(SHARED) utils.c main.c

all:
	make adidas && make unity && make light

unity:
	$(CC) -o $(TESTPN) $(TEST) $(SHARED) test/adidas_test.c
	./$(TESTPN)

clean:
	rm -f $(TESTPN);	
	rm -f $(PN);
	rm -f $(LPN);

light:
	$(CC) -o $(LPN) lightning/lightning.c lightning/main.c

.PHONY: all unity clean light adidas
