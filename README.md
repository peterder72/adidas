# ADIDAS encoding program

ADIDAS - **A**dvanced **D**evelopment for **I**nternet where all **D**ata **A**rrives **S**afely

Assignment for *Programming in C* class. Makes use of [parity bits](https://en.wikipedia.org/wiki/Parity_bit) for verifying data integrity and fixing possible errors.

## Structure

Main ADIDAS code is located in "_libadidas_" folder. Code for user interaction with ADIDAS is in "main.c" file.
Library for corrupting binaries is located in "_lightning_". "_lightning/main.c_" is used for user interaction with corruption library. Unity testing code is in "_test_" directory.

## Build

Requires *Unity* test library. Specify path to source in the makefile.

Use "_make_" command to build the binary for the ADIDAS encryption/decryption. Makefile also accepts different tagets:

1. make adidas (default) - build main binaries for ADIDAS
2. make light - build binary for flipping random bits
3. make unity - build Unity test suite and run it
4. make all - make all binaries
5. make clean - remove all binaries

## Usage

### ADIDAS

./adidas -d|-e [-i input file] [-o output file]

* -d - decode mode
* -e - encode mode

Input defaults to stdin and output defaults to stdout

### Corruption

./strike filename

Warning: specified file will be permanently damaged

## Unity

./unittest
