#include "libadidas.h"

// Here lies main adidas function

uint8_t adidas_encode(uint8_t n) {

    n &= 0xF;   // Truncating for safety


    /*
        Sum up bits in the order:
        0-1-3
        1-2-3
        2-0-3
    */
    for (int i = 0; i < 3; i++) {
        uint8_t sum = 0;

        sum += n >> i;
        sum += n >> ((i == 2) ? 0: i + 1);
        sum += n >> 3;

        // Parity bit is basically 
        // bit 0 from the sum
        // It is added to shifted position
        n |= (sum & 1) << i + 4;

    }

    return n;
}

uint8_t adidas_decode(uint8_t n) {

    uint8_t check_results = 0;

    /*
        Sum up bits with parity bits in the order:
        0-1-3-p0
        1-2-3-p1
        2-0-3-p2
    */
    for (int i = 0; i < 3; i++) {

        uint8_t sum = 0;

        sum += n >> i;
        sum += n >> ((i == 2) ? 0: i + 1);
        sum += n >> 3;
        sum += n >> i + 4;

        // Check whether LSB is 0 (parity is correct)
        // Store result as 1 bit in check_results
        // 1 - parity bit correct
        // 0 - parity bit incorrect
        check_results |= ((sum & 1) ^ 1) << i;
    
    }

    int ans = n & 0xF;

    switch (check_results) {
        // Parity bits are correct
        case 0b111:
            break;
        // Below are situations,
        // When 2 or 3 parity bits are incorrect.
        // Results are hardcoded
        // This could (possibly) be written as a loop,
        // but it would be even more bulky
        case 0b010:
            flip_bit(0, ans);
            break;
        case 0b100:
            flip_bit(1, ans);
            break;
        case 0b001:
            flip_bit(2, ans);
            break;
        case 0b000:
            flip_bit(3, ans);
            break;
        
        // One of the parity bits is incorrect
        default:
            // We're good to go,
            // as we don't care about
            // parity bits being correct
            break;
    }

    return ans;
}

// Wrapper around encode function
// to encode chars to 16-bit adidas-encoded vars
uint16_t adidas_encode_char(uint8_t c) {
    uint16_t ans = 0;

    ans += adidas_encode(c & 0xF);
    ans += adidas_encode((c >> 4) & 0xF) << 8;

    return ans;
}

// Wrapper around decode function
// to decode 16-bit encoded vars to chars
uint8_t adidas_decode_char(uint16_t encoded) {
    uint8_t ans = 0;

    ans += adidas_decode(encoded & 0xFF);
    ans += adidas_decode((encoded >> 8) & 0xFF) << 4;

    return ans;
}