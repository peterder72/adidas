#ifndef ADIDAS_H

#include <stdlib.h>
#include <stdint.h>

#include <stdio.h> // DEBUG ONLY

#define flip_bit(BIT, N) (N ^= 1 << BIT)

uint8_t adidas_encode(uint8_t number);
uint8_t adidas_decode(uint8_t number);

uint16_t adidas_encode_char(uint8_t c);
uint8_t adidas_decode_char(uint16_t number);

#endif