#ifndef UTILS_H
#define UTILS_H
#include "main.h"

void printHelp();
void encodeFile(FILE *in_f, FILE *out_f);
void decodeFile(FILE *inp_f, FILE *out_f);

#endif