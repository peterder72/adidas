#include "main.h"

FILE *inp;
FILE *out;

// Close input and output
// In the end
void cleanup() {
    if (inp != NULL) fclose(inp);
    if (out != NULL) fclose(out);
}

int main(int argc, char **argv) {

    atexit(cleanup);

    ad_mode_t mode = NONE;
    static struct timeval start;

    // Default input and output
    inp = stdin;
    out = stdout;

    int argi = 0;

    // CLI argument processor
    while (++argi < argc) {

        // Print help string
        if (strcmp(argv[argi], "-h") == 0) {
            printHelp();
            exit(0);
        } else if (strcmp(argv[argi], "-i") == 0) { // Specify input
            inp = fopen(argv[++argi], "r");
            if (inp == NULL) {
                fprintf(stderr, "Error: could not open file %s\n", argv[argi]);
                exit(1);
            } else {
                fprintf(stdout, "Opened file \"%s\" for input\n", argv[argi]);
            }
        } else if (strcmp(argv[argi], "-o") == 0) { // Specify output
            out = fopen(argv[++argi], "w");
            if (out == NULL) {
                fprintf(stderr, "Error: could not open file %s\n", argv[argi]);
                exit(1);
            } else {
                fprintf(stdout, "Opened file \"%s\" for output\n", argv[argi]);
            }
        } else if (strcmp(argv[argi], "-d") == 0) { // Decode mode
            mode = DECODE;
        } else if (strcmp(argv[argi], "-e") == 0) { // Encode mode
            mode = ENCODE;
        }
    }

    // Mode must be specified
    if (mode == NONE) {
        fprintf(stderr, "Error: no mode specified\n");
        printHelp();
        exit(1);
    }

    printf("Selected mode: %s\n", (mode == ENCODE) ? "Encode" : "Decode");

    // Abstract function for encoding or decoding file
    void (*adidas_f)(FILE *, FILE *) = (mode == ENCODE) ? encodeFile : decodeFile; 

    // Start timer for the thing
    gettimeofday(&start, NULL);

    // Main part here
    adidas_f(inp, out);

    struct timeval end;

    // Stopping timer, displaying time
    gettimeofday(&end, NULL);
    unsigned long long t = 1000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000;

    fprintf(stdout, "Completed in %lld ms\n", t);

    return 0;

}